package iu.winfo.qltymng.customernumbergenerator.services;

import iu.winfo.qltymng.customernumbergenerator.models.CustomerNumber;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class NumberGenerator {
    List<CustomerNumber> existingNumbers;

    public NumberGenerator(){
        existingNumbers = new ArrayList<>();
    }

    public CustomerNumber generateNumber(Optional<String> prefix, int number) throws Exception {
        CustomerNumber customerNumber = new CustomerNumber(prefix,number);
        if(!existingNumbers.contains(customerNumber)){
            existingNumbers.add(customerNumber);
            return customerNumber;
        }
        if(existingNumbers.contains(customerNumber)){
            number = number + 1;
            if(number >= 1000000){
                number = 100000;
                prefix = Optional.of(countUpPrefix(customerNumber.getPrefix(), 1));
            }
            customerNumber = generateNumber(prefix,number);
        }
        else {
            throw new Exception();
        }
        return customerNumber;
    }

    private String countUpPrefix(String prefix, int index) throws Exception {
        char[] prefixArray = prefix.toCharArray();
        char lastChar;
        try {
            lastChar = prefixArray[prefix.length() - index];
        }
        catch (IndexOutOfBoundsException ex){
            throw new Exception();
        }
        try {
            int asciInt = (int)lastChar;
            asciInt++;
            if(asciInt > 90 || asciInt < 65){
                prefixArray[prefix.length() - index] = (char)65;
                return countUpPrefix(new String(prefixArray), index + 1);
            }
            lastChar = (char) asciInt;
            prefixArray[prefix.length() - index] = lastChar;
            return new String(prefixArray);
        }catch (Exception ex){
            throw ex;
        }
    }
}
