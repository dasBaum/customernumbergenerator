package iu.winfo.qltymng.customernumbergenerator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerNumberGeneratorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomerNumberGeneratorApplication.class, args);
    }

}
