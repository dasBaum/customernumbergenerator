package iu.winfo.qltymng.customernumbergenerator.models;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Optional;

@Data
public class CustomerNumber {
    String prefix;
    int number;

    public CustomerNumber(Optional<String> prefix,int number) throws Exception {
        if(prefix.isPresent()){
            String tempPrefix = prefix.get().toUpperCase();
            if(tempPrefix.length() != 2 || !tempPrefix.chars().allMatch(Character::isLetter)){
                throw new Exception();
            }
            else {
                this.prefix = tempPrefix;
            }
        }
        else {
            this.prefix = "AA";
        }
        if(number >= 100000 && number <= 999999){
            this.number = number;
        }
        else {
            throw new Exception();
        }
    }

    @Override
    public String toString(){
        return prefix + " " + number;
    }
}
