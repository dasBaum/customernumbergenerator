package iu.winfo.qltymng.customernumbergenerator.tests;

import iu.winfo.qltymng.customernumbergenerator.models.CustomerNumber;
import iu.winfo.qltymng.customernumbergenerator.services.NumberGenerator;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import org.yaml.snakeyaml.nodes.ScalarNode;

import java.util.Optional;
import java.util.Random;
import java.util.Scanner;

@SpringBootTest
public class ModuleTest {

    @Autowired
    private NumberGenerator numberGenerator;

    @Test
    public void TestData() throws Exception {
            CustomerNumber number = numberGenerator.generateNumber(Optional.of("ab"),999999);
            System.out.println(number);
            number = numberGenerator.generateNumber(Optional.of("az"),999999);
            System.out.println(number);
            for (int i = 0; i < 3;i++){
                int randomInt = 100000 + (int)(Math.random() * ((1000000 - 100000) + 1));
                number = numberGenerator.generateNumber(Optional.empty(),randomInt);
                System.out.println(number);
            }
            number = numberGenerator.generateNumber(Optional.of("AZ"),999999);
            System.out.println(number);
    }

    @Test
    public void AddIncremental() throws Exception {
        for (int i = 0; i <= 10000;i++){
            CustomerNumber number  = numberGenerator.generateNumber(Optional.empty(),100000 + i);
            System.out.println(number);
        }
    }


    @Test
    public void AddRandom() throws Exception {
        for (int i = 0;i <= 1000;i++){
            int randomInt = 100000 + (int)(Math.random() * ((1000000 - 100000) + 1));
            CustomerNumber number = numberGenerator.generateNumber(Optional.of(createRandomPrefix()),randomInt);
            System.out.println(number);
        }
    }

    @Test
    public void CustomTest() throws Exception {
        for (int i = 0; i < 100;i++){
            CustomerNumber number = numberGenerator.generateNumber(Optional.of("AA"),100000);
            System.out.println(number);
        }
    }

    private String createRandomPrefix(){
        Random random = new Random();
        char[] prefixArr = new char[2];
        prefixArr[0] = (char) (random.nextInt(26) + 'a');
        prefixArr[1] = (char) (random.nextInt(26) + 'a');
        return new String(prefixArr);
    };
}
